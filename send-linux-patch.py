#!/usr/bin/python3

# This script is to collect the following functions
#    1. Check patch style. It exits if hit a style problem.
#    2. If no style problems, it will get all email addresses
#       that related to your patches.
#    3. Construct the git command to send your patches. Then
#       No need to make the git send command manually.
#    4* Plan to add spelling check.
#
#    Author: Xiongwei Song <xiongwei.song@windriver.com>
#    Date: 2023/12/18

import subprocess
import glob, sys
import os
import re

print(sys.argv[1:])
# check patch style
err_num = re.compile("[1-9][0-9]* (errors|warnings)")
for patch in sys.argv[1:]:
    print("Checking " + patch)
    retcode = subprocess.run(["./scripts/checkpatch.pl", patch], capture_output=True, text=True)
    m = err_num.search(retcode.stdout)
    if m != None:
        print(m)
        print(retcode.stdout)
        exit(1)
    else:
        print("Passed...")

# get maintainers
diff_path = os.path.dirname(os.path.abspath(sys.argv[1])) + '/*.patch'
retcode = subprocess.run(["./scripts/get_maintainer.pl" + " " + diff_path], shell=True, capture_output=True, text=True)

# construct git send command
git_send = 'git send-email --from="Xiongwei Song <sxwjean@me.com>"'
git_to = "--to="
git_cc = "--cc="
email_addr = '[A-za-z0-9._%+-]+@[A-Za-z0-9.-]+(\.[A-Za-z]{2,})+'
email_list = '[A-za-z0-9._%+-]+@(vger\.kernel\.org|kvack\.org)'
lines = retcode.stdout.splitlines()
    
for line in lines:
    m_addr = re.search(email_addr, line)
    m_list = re.search(email_list, line)
    if m_addr != None:
        if m_list == None:
            git_to += m_addr.group() + ","
        else:
            git_cc += m_list.group() + ","

# remove the last comma
git_to = git_to[:-1]
git_cc = git_cc[:-1]

print(git_send + " " + git_to + " " + git_cc + " " + diff_path)
